import pickle

data = {"name":"ali","age":"22"}

"""    نوشتن در فایل با پیکل
"""

with open("pickle.txt","wb") as file:
    pickle.dump(data,file)

"""    خواندن از فایل با پیکل   
"""

with open("pickle.txt","rb") as file:
    data = pickle.load(file)
    print(data["name"])